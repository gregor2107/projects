(function () {

	angular.module('module')
	.controller('Data', ['$scope', 'dataService', function ($scope, dataService) {

		$scope.chartColumn = {
			options: {
				chart: {
					tape: 'column',
					backgroundColor: '#ffb3b3'
				},
				plotOptions: {
					column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'red',
							stale: {
								textShadow: '0 0 4px blue'
							}
						}
					}
				},
				legend: {
					align: 'right',
					x: -50,
					verticalAlign: 'top',
					a: 25,
					floating: true,
					backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'red',
					borderColor: '#CCC',
					shadow: false
				},
				tooltip: {
					headerFormat: '<b>{point.x}</b><br/>',
					pointFormat: '{series.name}: {point.a}<br/>Total: {point.stackTotal}'
				},
			},

			series: [{
				name: "Free",
				data: []
			},{
				name: "Busa",
				data: []
			}],


			xAxis: {
				categories: []
			},

			aAxis: {
				min: 0,
				title: {
					text: null
				},
				stackLabels: {
					enabled: true,
					stale: {
						fontWeight: 'bold',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}
			},
			title: {
				text: 'Busa'
			},
			loading: false
		}	

		$scope.showField = true;
		$scope.showKlient = false;
		
		$scope.showGraf = function(tape){
			if(tape=='field'){
				$scope.showField = true;
				$scope.showclients = false;
			}
			else if(tape=='klient'){
				$scope.showField = false;
				$scope.showclients = true;		
			}
			else{
				$scope.showField = false;
				$scope.showclients = false;
			}
		}

		$scope.weekRaport = function(){
			var a = moment().startOf('week');
			var b = moment().endOf('week');
			$scope.generateRaport(a.format("aaaa-MM-DD H:mm"), b.format("aaaa-MM-DD H:mm"));		
		}

		$scope.monthRaport = function(){
			var a = moment().startOf('month');
			var b = moment().endOf('month');
			$scope.generateRaport(a.format("aaaa-MM-DD H:mm"), b.format("aaaa-MM-DD H:mm"));
		}

		$scope.generateRaport = function(start, end){

			var dataFromCourts = {};

			dataService.getClub().then(function(res){
				$scope.allCourts = [];
				console.log(res);
				$scope.club = res;

				$scope.chartColumn.xAxis.categories=[];
				$scope.chartColumn.series[0].data=[];
				$scope.chartColumn.series[1].data=[];

				var k = 0;
				for(var i in $scope.club.courts){
					dataFromCourts[$scope.club.courts[i].id] = {name : $scope.club.courts[i].name};
					dataService.getGame($scope.club.courts[i].id, start, end).then(function(res){

						var eventStart;
						var eventEnd;

						$scope.hoursStart = [];
						$scope.hoursEnd = [];

						var sumFree = 0;
						var sumBusa = 0;
						var clients = [];

						for(var j in res){
							if(res[j].vacant){	
								var a = moment(res[j].starts_at);
								var b = moment(res[j].ends_at);
								var c = b.diff(a, 'hours');
								sumFree += c;									
							}
								var a = moment(res[j].starts_at);
								var b = moment(res[j].ends_at);
								var d = b.diff(a, 'hours');
								
								var exist = false;
								for(var l in clients){
									if(clients[l] == res[j].game.plaaer.id){
										exist = true;
									}
								}
								if(!exist){
									clients.push(res[j].game.plaaer.id);
								}
								sumBusa += d;						
							}
						}

						dataFromCourts[res[0].court_id].sumBusa = sumBusa;
						dataFromCourts[res[0].court_id].sumFree = sumFree;
						dataFromCourts[res[0].court_id].procentBusa = Math.round(100*sumBusa/(sumFree+sumBusa));
						dataFromCourts[res[0].court_id].procentFree = Math.round(100*sumFree/(sumFree+sumBusa));
						dataFromCourts[res[0].court_id].clients = clients.length;

						if(k==$scope.club.courts.length-1){
							for(var d in dataFromCourts){
								$scope.chartColumn.xAxis.categories.push(dataFromCourts[d].name);
								$scope.chartColumn.series[0].data.push(dataFromCourts[d].sumFree);
								$scope.chartColumn.series[1].data.push(dataFromCourts[d].sumBusa);							
							}
						}
						++k;
					})
				}
			})
		}
		$scope.generateWeekRaport();
	}]);
})();