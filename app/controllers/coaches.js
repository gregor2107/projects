(function () {

	angular.module('module')
		.controller('Coach', [ '$sessionStorage', 'dataService', '$scope', function ($sessionStorage, dataService, $scope) {

			$scope.minlength = 8;
			dataService.getUsers().then(function(res){
				$scope.klient = res;
				for (var i in res){
					if(!res[i].avatar){
						$scope.klient[i].avatar = 'avatar';
					}
				}
			})

			$scope.getCoaches = function(){
				dataService.getCoaches().then(function(res){
					$scope.trener = res;
					var tmp = [];
					for (var i in res){
						if(!res[i].avatar){
							$scope.coachs[i].avatar = 'avatar';
						}
						$scope.trener[i].admin = String(res[i].admin);
						if($scope.trener[i].admin == $sessionStorage.admin){
							tmp.push($scope.trener[i]);
						}
					}
					$scope.coachs = tmp;
				})
			}
			
			$scope.getUserEmail = function(usersMail){
				dataService.getUserEmail(usersMail).then(function(res){

					if(res.length != 0){
						$scope.usersMail = true;
					}
					else{
						$scope.usersMail = false;
					}
				})
			}
			
		    $scope.trenerSelect = function(select){
		    	$scope.choseCoach = {};
		    	$scope.choseCoach = select;
		    }

			dataService.getClubs().then(function(res){
				$scope.clubs = res;
			})
			
			$scope.showModal = function(coach){
				$scope.choseCoach = {};
			 	if(coach){
					$scope.choseCoach = coach;
					$scope.editCoach = true;
				}
				else{
					$scope.editCoach =  false;
					if($sessionStorage.admin)
						$scope.choseCoach.admin = String($sessionStorage.admin);
				}

			    this.dialogShown.coaches = true;
		    }

			$scope.saveUser = function(choseCoach){
					var params = {
						first_name: choseCoach.first_name,
						last_name: choseCoach.last_name,
						birthdate: String(choseCoach.birthdate),
						phone_number: String(choseCoach.phone_number),
						email: choseCoach.email,
						level: choseCoach.level
					};
					dataService.saveUsers(params,choseCoach.id);
		    }
		}]);
})();