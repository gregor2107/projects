angular.module('module')
  .controller('loggingin', function ($sessionStorage, $location, $rootScope, dataService, $scope) {
      
	  if ($sessionStorage.logging) {
        if($sessionStorage.admin){
          $location.path('/panel');
        }
        else{
          $location.path('/clients');
        }
      }

  		$scope.email = '';
  		$scope.pass = '';

    	$scope.submit = function () {
			if($scope.loggingin.$valid){
				var params = {};
				params.email = $scope.email;
				params.pass = $scope.pass;

				dataService.logginginAdmin(params).then(function(res){
						$sessionStorage.access_token = res.access_token;
						$scope.errorString = "";
						if(!res.error){
						  $sessionStorage.logging = true;
						  $rootScope.logging = $sessionStorage.logging;
						  $sessionStorage.admin = res.admin;
						  $rootScope.admin = res.admin;

						  if($sessionStorage.admin){
							$location.path('/panel');
						  }
						}
						else{
						  $scope.errorString = res.error;
						};
				});
			}
    	};

    	this.validateUser = function (email, pass) {
    		if (email === this.auth.email && pass === this.auth.pass) {
    			$sessionStorage.logging = true;
				$rootScope.logging = $sessionStorage.logging;
    			$location.path('/');
    		} else {
    			console.log('Auth failed');
    		}
    	};
  });