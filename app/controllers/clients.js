(function () {

	angular.module('module')
		.controller('Clients', [ '$sessionStorage', 'dataService', '$scope', function ($sessionStorage, dataService, $scope) {
			

			$scope.dialogShown = {};
		    $scope.dialogShown.user = false;
		    $scope.usersReadonly = false;

		    if($sessionStorage.club_id != null){
				$scope.usersReadonly = true;
			}

			$scope.getUsers = function(){
				dataService.getUsers().then(function(res){
					$scope.klient = res;
					for (var i in res){
						if(!res[i].avatar){
							$scope.klient[i].avatar = '';
						}
					}	
				})
			}

			$scope.ageCounter = function(){  	
		    	var a = moment();
		    	var b = $scope.pageClient.birthdate;
				var age = a.diff(b, 'years');
	  		
				if(age<18){
					$scope.pageClient.seniority_level = 'Junior';
				}else if(age>=18 && age<=36){
					$scope.pageClient.seniority_level = 'Adult';
				}else if(age>=7 && age<=48){
					$scope.pageClient.seniority_level = 'Young Senior';
				}else if(age>=49 && age<=64){
					$scope.pageClient.seniority_level = 'Senior';
				}else if(age>=65){
				 	$scope.pageClient.seniority_level = 'Super Senior';
				}
		    }

			$scope.showModal = function(formType, klienci){
					$scope.pageClient = klienci;		    
			        this.dialogShown.user = true;     
		    }

		    $scope.saveUser = function(pageClient){
					var params = {
						first_name: pageClient.first_name,
						last_name: pageClient.last_name,
						birthdate: String(pageClient.birthdate),
						seniority_level: pageClient.seniority_level,
						phone_number: String(pageClient.phone_number),
						email: pageClient.email,
						level: pageClient.level
					};

					dataService.saveUsers(params,pageClient.id).then(function(res){
						$scope.getUsers();
					});		
			    	$scope.close();
		    }

		    $scope.close = function(){
		    	$scope.dialogShown.user = false;
		    	$scope.save = {};
		    }

		}]);
})();