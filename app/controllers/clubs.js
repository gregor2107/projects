(function () {
	'use strict';

	angular.module('module')
		.controller('Clubs', [ '$window', '$sessionStorage', 'dataService', '$scope', function ($window, $sessionStorage, dataService, $scope) {

			$scope.dialogShown = {};
		    $scope.dialogShown.club = false;
		    $scope.dialogShown.court = false;

		    if($sessionStorage.club_id != null){
				$scope.clubReadonly = true;
			}

			dataService.getClubs().then(function(res){
				$scope.clubs = res;
			})

			$scope.getClubs = function(){
				dataService.getClubs().then(function(res){
					$scope.clubs = res;
					$scope.file = res.avatar;
				})
			}

			dataService.getImages($sessionStorage.club_id).then(function(res){
				$scope.image = res;
			})

			$scope.surface = [{
				name:"Trawa",
				value:"grass"
			},
			{
				name:"Glina",
				value:"clay"
			},
			{
				name:"Twarda",
				value:"hard"
			},
			]

			$scope.showModal = function(formType, club){
				$scope.pageClub = {};
				$scope.pageClub = club;		    
			       
			 	if(club){
					$scope.pageClub = club;
					$scope.editClub = true;
					dataService.getImages(club.id).then(function(res){
						$scope.image = res;
					})
				}
				else if(!club)
					$scope.editClub =  false;
			 	this.dialogShown.club = true;

		    }
		    $scope.showModal2 = function(court){		
			 	this.dialogShown.court = true;
		    }

		    $scope.saveClub = function(pageClub){
				var id = pageClub.id;
				var formData = new FormData();

				var params = {
					name: pageClub.name,
					email: pageClub.email,
					website: pageClub.website,
					description: pageClub.description
				};

				if( $scope.avatarToUpload){
					params.avatar=$scope.avatarToUpload;
				}
				for(var i in params){
					formData.append('club['+i+']', params[i]);
				}

				var location_attributes = {
					address_line_1 : pageClub.location.address_line_1,
					lat: 20,
					lng: 50
				}

				for(var i in location_attributes){
					formData.append('club[location_attributes]['+i+']', location_attributes[i]);
				}
		    }

		    $scope.close = function(){
		    	$scope.dialogShown.club = false;
		    	$scope.dialogShown.court = false;
		    	$scope.save = {};
		    }

		    $scope.selectFile = function()
		    {
		        $("#file").click();
		    }

		    $scope.selectFile2 = function($files)
		    {
		        $("#files").click();
		        $scope.galleryFiles = $files;
		    }

		    $scope.fileNameChaged = function(files)
		    {	
				$scope.avatarToUpload = files;
		    }
		}]);
})();