/*
 * Angular 2 decorators and services
 */
import {Component, ViewEncapsulation, ViewContainerRef} from '@angular/core';

import {AppState} from './app.service';

import {Overlay} from 'angular2-modal';
import {Modal} from 'angular2-modal/plugins/bootstrap';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.style.css'
  ],
  template: `
    <main>
      <router-outlet></router-outlet>
    </main>
    <section>
    <ul class="app--main__main-list">
      <li *ngFor="let json of this.jsons;" (click)="this.clickOnJson(json)">
          {{json.name}}
       </li>
    </ul>
    <ul *ngIf="checkSize()" class="app--main__main-checked-list">    
      <li>
        <span>id:</span>
        <strong>{{this.checked.id}} </strong>
      </li>
      
      <li>
        <span>name:</span>
        <strong>{{this.checked.name}} </strong>
       </li>
      <li>
        <span>description:</span>
        <strong>{{this.checked.description}} </strong>
       </li>
       <li>
          <span>unit:</span>
          <strong>{{this.checked.unit}} </strong>
       </li>
    </ul>
    </section>
  `
})
export class App {
  checked: Object;
  jsons: Array<any>;

  constructor(overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal,
              public appState: AppState) {
    overlay.defaultViewContainer = vcRef;

    this.checked = [];
    this.jsons = [
      {
        id: 1,
        name: 'json1',
        description: 'description1',
        unit: 'unit1'
      },
      {
        id: 2,
        name: 'json2',
        description: 'description2',
        unit: 'unit2'
      },
      {
        id: 3,
        name: 'json3',
        description: 'description3',
        unit: 'unit3'
      },
    ]


  }

  clickOnJson(json) {
    this.checked = json;


    this.modal.alert()
      .size('lg')
      .title('Modal with selected data')
      .body(`
        <ul class="app--main__main-checked-list">    
        <li>
          <span>id:</span>
          <strong>${this.checked.id} </strong>
        </li>
        
        <li>
          <span>name:</span>
          <strong>${this.checked.name} </strong>
         </li>
        <li>
          <span>description:</span>
          <strong>${this.checked.description}</strong>
         </li>
         <li>
            <span>unit:</span>
            <strong>${this.checked.unit} </strong>
         </li>
      </ul>
            `)
      .open();
  }

  checkSize() {
    return (Object.keys(this.checked).length > 0);
  }

}

/*
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
