import React, {Component} from 'react';
import {Button, Col, Row} from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';

class Counter extends Component {

    constructor() {
        super();
        this.state = {
            count: 1,
            fizzBuzz: '',
            dynamicColor: '',
        }
    }

    render() {
        const {dynamicColor, fizzBuzz, count} = this.state;

        return (
            <Row>
                <Col sm={12} className="text-center">
                    <h2
                        className={`default ${dynamicColor}`}>
                        {fizzBuzz || count}
                    </h2>
                    <div className="btn-group">
                        <Button className="btn" onClick={this.decrement.bind(this)}>
                            <FontAwesome name="minus"/>
                        </Button>
                        <Button bsStyle="success" className="btn" onClick={this.increment.bind(this)}>
                            <FontAwesome name='plus'/>
                        </Button>
                    </div>
                </Col>
            </Row>
        );
    }


    fizzBuzz(num) {
        if (num % 15 === 0) {
            this.setState({fizzBuzz: 'FizzBuzz', dynamicColor: 'green'});
        } else if (num % 5 === 0) {
            this.setState({fizzBuzz: 'Buzz', dynamicColor: 'blue'});
        } else if (num % 3 === 0) {
            this.setState({fizzBuzz: 'Fizz', dynamicColor: 'yellow'});
        } else {
            this.setState({fizzBuzz: '', dynamicColor: 'defaultColor'});
        }
    }

    decrement() {
        if (this.state.count > 1) {
            this.setState({count: --this.state.count});
            this.fizzBuzz(this.state.count);
        }
    }

    increment() {
        if (this.state.count < 1000) {
            this.setState({count: ++this.state.count});
            this.fizzBuzz(this.state.count);
        }
    }
}

export default Counter;