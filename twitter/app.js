(function() {
  var app = angular.module('panel', ['pascalprecht.translate']);


  app.config(function($translateProvider) {
      $translateProvider.translations('en', {
          HEADLINE: 'Hello there, This is my awesome app!',
          INTRO_TEXT: 'And it has i18n support!',
          BUTTON_TEXT_EN: 'english',
          BUTTON_TEXT_DE: 'german'
        })
        .translations('de', {
          HEADLINE: 'Hey, das ist meine großartige App!',
          INTRO_TEXT: 'Und sie untersützt mehrere Sprachen!',
          BUTTON_TEXT_EN: 'englisch',
          BUTTON_TEXT_DE: 'deutsch'
        });
        $translateProvider.preferredLanguage('en');
    });

  app.controller('TranslateController', function($translate, $scope) {
          $scope.changeLanguage = function (langKey) {
          $translate.use(langKey);
      };
    });


  app.controller('StoreController', function() {
      this.products = gems;
    });

  app.controller("PanelController", function(){
      this.tab = 1;

      this.selectTab = function(setTab){
        this.tab = setTab;
      };

      this.isSelected = function(checkTab){
        return this.tab === checkTab;
      };

    });

  app.controller("ReviewController", function(){

    this.review = {};

    this.addReview = function(product){
      this.review.createdOn = Date.now();
      product.reviews.push(this.review);
      this.review = {};
    };
  });

  var gems = [{
      name: '',
      description: "",
      shine: 8,
      price: 110.50,
      reviews: []
    }];

})();