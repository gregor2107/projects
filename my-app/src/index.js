import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import items from './reducers/storeReducers';


let reducers = combineReducers(
  {
    items
  }
)

let store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
debugger;
ReactDOM.render(
  <div>
    <Provider store={store}>
    <App />
    </Provider>
  </div>,
  document.getElementById('root')
);
