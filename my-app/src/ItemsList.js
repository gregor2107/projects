import React from 'react';
import Item from './Item';
import {connect} from 'react-redux';

const items = [
    {
        id: 1,
        name: 'Pomidory',
        type: 'warzywo',
        country: 'Polska'
    },
    {
        id: 2,
        name: 'Ogórki',
        type: 'warzywo',
        country: 'Polska'
    },
    {
        id: 3,
        name: 'Banany',
        type: 'owoc',
        country: 'Brazylia'
    }
]

class ItemsList extends React.Component{
    render() {
        console.log(this.props);
        return(
            <div>
            {
                this.props.items.map((item) => {
                    return <Item id={item.id} name={item.name} key={item.id} />
                })
            }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
}

export default connect(mapStateToProps)(ItemsList);