import ActionTypes from './../ActionTypes';

export function search(query){
    return{
        type: 'ActionTypes.SEARCH',
        payload: {
            query: query
        }
    }
}

