import ActionTypes from './../ActionTypes';

const initialData = [
    {
        id: 1,
        name: 'Pomidory',
        type: 'warzywo',
        country: 'Polska'
    },
    {
        id: 2,
        name: 'Ogórki',
        type: 'warzywo',
        country: 'Polska'
    },
    {
        id: 3,
        name: 'Banany',
        type: 'owoc',
        country: 'Brazylia'
    }
];

export const storeReducer = (store=initialData, action) => {
    switch(action.type) {
        case ActionTypes.SEARCH: {
            return store;
        }
        default: {
            return store;
        }
    }
}

export default storeReducer;